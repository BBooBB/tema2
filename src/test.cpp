#include<iostream>
#include<fstream>
#include<string>
using namespace std;
int main()
{
	//variabila neinitializata
	int var_neinit=10;
	cout << "Variabila neinitializata=" << var_neinit << endl;

	//variabila nefolosita
	int a=var_neinit;
	cout << "var_neint/2=" << a / 2 << endl;

	//pierdere de resurse
	ifstream fis("in.txt");
	string s;
	fis >> s;
	ofstream fis_out("out.txt");
	fis_out << s;
	fis.close();
	fis_out.close();

	//accesare index inexistent
	int *sir = new int[5];
	sir[4] = 7;
	delete[] sir;

	//memory leak
	for (int i = 0; i < 3; i++)
	{
		int *b = new int[5];
		delete[] b;
	}

	//dead code
	cout << "Reachable code" << endl;
	return 0;
}