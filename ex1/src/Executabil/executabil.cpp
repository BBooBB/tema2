#include "stdafx.h"
#include <windows.h>
#include "sl.h"
typedef void(__cdecl *OnStartProcedure)();

void main()
{
	HINSTANCE hModule1 = LoadLibrary(TEXT("dll1.dll"));
	HINSTANCE hModule2 = LoadLibrary(TEXT("dll2.dll"));


	OnStartProcedure pOnStart1 = (OnStartProcedure)GetProcAddress(hModule1, "OnStart1");
	OnStartProcedure pOnStart2 = (OnStartProcedure)GetProcAddress(hModule2, "OnStart2");

	if (pOnStart1)
		pOnStart1();

	if (pOnStart2)
		pOnStart2();

	OnStart3();
	
	FreeLibrary(hModule1);
	FreeLibrary(hModule2);
	
}

