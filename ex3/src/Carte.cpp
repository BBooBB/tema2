#include "Carte.h"
#include <iostream>
#include <map>
#include <hash_map>
using namespace std;

Carte::Carte()
{}
Carte::Carte(string text)
{
	this->text = text;
}
void Carte::initializare()
{
	char *word = strtok(const_cast<char*> (text.c_str()), " .,?!");
	while (word)
	{
		int gasit = 0;
		for (map<string, int>::iterator m = wordsMap.begin(); m != wordsMap.end();++m)
			if (m->first == word)
			{
				gasit = 1;
				break;
			}
		if (!gasit)
			wordsMap.insert(pair<string, int>(word, 1));
		else
		{
			map<string, int>::iterator m = wordsMap.find(word);
			int val = m->second;
			m->second = val + 1;
		}

		word = strtok(NULL, " .,-?!");
	}
	for (map<string, int>::iterator m = wordsMap.begin(); m != wordsMap.end(); ++m)
		cout << " first: " << m->first << " second: "<< m->second<<endl;
}

string Carte::cuvantFavorit()
{
	int max = 0;
	string word="";
	for (map<string, int>::iterator m = wordsMap.begin(); m != wordsMap.end(); ++m)
		if (m->second > max)
		{
		max = m->second;
		word = m->first;
		}
	return word;
}

void Carte::cuvAparitieN(int n)
{
	for (map<string, int>::iterator m = wordsMap.begin(); m != wordsMap.end(); ++m)
		if (m->second > n)
			cout << m->first;
}

